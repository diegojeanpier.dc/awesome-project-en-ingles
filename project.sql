-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema projectJuanDiego
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema projectJuanDiego
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `projectJuanDiego` DEFAULT CHARACTER SET utf8 ;
USE `projectJuanDiego` ;

-- -----------------------------------------------------
-- Table `projectJuanDiego`.`student`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projectJuanDiego`.`student` (
  `idstudent` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `lastname` VARCHAR(100) NOT NULL,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`idstudent`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projectJuanDiego`.`teacher`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projectJuanDiego`.`teacher` (
  `idteacher` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `lastname` VARCHAR(100) NOT NULL,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`idteacher`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projectJuanDiego`.`subject`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projectJuanDiego`.`subject` (
  `idsubject` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idsubject`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projectJuanDiego`.`groups`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projectJuanDiego`.`groups` (
  `idgroups` INT NOT NULL,
  `groupnumber` INT NOT NULL,
  PRIMARY KEY (`idgroups`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projectJuanDiego`.`group_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projectJuanDiego`.`group_detail` (
  `id_detail_group` INT NOT NULL AUTO_INCREMENT,
  `idgroups` INT NOT NULL,
  `idstudent` INT NOT NULL,
  INDEX `fk_student_has_groups_groups1_idx` (`idgroups` ASC),
  INDEX `fk_student_has_groups_student_idx` (`idstudent` ASC),
  PRIMARY KEY (`id_detail_group`),
  CONSTRAINT `fk_student_has_groups_student`
    FOREIGN KEY (`idstudent`)
    REFERENCES `projectJuanDiego`.`student` (`idstudent`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_has_groups_groups1`
    FOREIGN KEY (`idgroups`)
    REFERENCES `projectJuanDiego`.`groups` (`idgroups`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projectJuanDiego`.`delivereables`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projectJuanDiego`.`delivereables` (
  `iddelivereables` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `subject_idsubject` INT NOT NULL,
  `deadline` DATE NOT NULL,
  PRIMARY KEY (`iddelivereables`),
  INDEX `fk_delivereables_subject1_idx` (`subject_idsubject` ASC),
  CONSTRAINT `fk_delivereables_subject1`
    FOREIGN KEY (`subject_idsubject`)
    REFERENCES `projectJuanDiego`.`subject` (`idsubject`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projectJuanDiego`.`subjects_students`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projectJuanDiego`.`subjects_students` (
  `id_subjects_students` VARCHAR(45) NOT NULL,
  `idstudent` INT NOT NULL,
  `idsubject` INT NOT NULL,
  PRIMARY KEY (`id_subjects_students`),
  INDEX `fk_student_has_subject_subject1_idx` (`idsubject` ASC),
  INDEX `fk_student_has_subject_student1_idx` (`idstudent` ASC),
  CONSTRAINT `fk_student_has_subject_student1`
    FOREIGN KEY (`idstudent`)
    REFERENCES `projectJuanDiego`.`student` (`idstudent`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_has_subject_subject1`
    FOREIGN KEY (`idsubject`)
    REFERENCES `projectJuanDiego`.`subject` (`idsubject`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projectJuanDiego`.`subjects_teacher`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projectJuanDiego`.`subjects_teacher` (
  `idsubject` INT NOT NULL,
  `idteacher` INT NOT NULL,
  `id_subjects_teacher` INT NOT NULL,
  INDEX `fk_subject_has_teacher_teacher1_idx` (`idteacher` ASC),
  INDEX `fk_subject_has_teacher_subject1_idx` (`idsubject` ASC),
  PRIMARY KEY (`id_subjects_teacher`),
  CONSTRAINT `fk_subject_has_teacher_subject1`
    FOREIGN KEY (`idsubject`)
    REFERENCES `projectJuanDiego`.`subject` (`idsubject`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_subject_has_teacher_teacher1`
    FOREIGN KEY (`idteacher`)
    REFERENCES `projectJuanDiego`.`teacher` (`idteacher`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projectJuanDiego`.`groups_delivereables`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projectJuanDiego`.`groups_delivereables` (
  `idgroups` INT NOT NULL,
  `iddelivereables` INT NOT NULL,
  `score` VARCHAR(45) NULL,
  `id_groups_delivereables` INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_groups_delivereables`),
  INDEX `fk_groups_has_delivereables_delivereables1_idx` (`iddelivereables` ASC),
  INDEX `fk_groups_has_delivereables_groups1_idx` (`idgroups` ASC),
  CONSTRAINT `fk_groups_has_delivereables_groups1`
    FOREIGN KEY (`idgroups`)
    REFERENCES `projectJuanDiego`.`groups` (`idgroups`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_groups_has_delivereables_delivereables1`
    FOREIGN KEY (`iddelivereables`)
    REFERENCES `projectJuanDiego`.`delivereables` (`iddelivereables`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projectJuanDiego`.`file`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projectJuanDiego`.`file` (
  `idfile` INT NOT NULL AUTO_INCREMENT,
  `route` VARCHAR(100) NOT NULL,
  `delivery_date` TIMESTAMP NOT NULL,
  `groups_delivereables_id_groups_delivereables` INT NOT NULL,
  PRIMARY KEY (`idfile`),
  INDEX `fk_file_groups_delivereables1_idx` (`groups_delivereables_id_groups_delivereables` ASC),
  CONSTRAINT `fk_file_groups_delivereables1`
    FOREIGN KEY (`groups_delivereables_id_groups_delivereables`)
    REFERENCES `projectJuanDiego`.`groups_delivereables` (`id_groups_delivereables`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projectJuanDiego`.`individual_score`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projectJuanDiego`.`individual_score` (
  `id_individual_score` INT NOT NULL AUTO_INCREMENT,
  `idstudent` INT NOT NULL,
  `iddelivereables` INT NOT NULL,
  `score` INT NULL,
  PRIMARY KEY (`id_individual_score`),
  INDEX `fk_student_has_delivereables_delivereables1_idx` (`iddelivereables` ASC),
  INDEX `fk_student_has_delivereables_student1_idx` (`idstudent` ASC),
  CONSTRAINT `fk_student_has_delivereables_student1`
    FOREIGN KEY (`idstudent`)
    REFERENCES `projectJuanDiego`.`student` (`idstudent`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_has_delivereables_delivereables1`
    FOREIGN KEY (`iddelivereables`)
    REFERENCES `projectJuanDiego`.`delivereables` (`iddelivereables`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
