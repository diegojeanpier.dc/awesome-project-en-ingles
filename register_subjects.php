<?php 
session_start();

if(isset($_SESSION['user_teacher'])){
    $username = $_SESSION['user_teacher'];
    ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Register Subjects</title>

        <!-- Google font -->

        <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <!-- font awesome -->
        <script src="https://kit.fontawesome.com/e70d054944.js"></script>

        <!-- style -->

        <style>
            body {
                font-family: 'Ubuntu', sans-serif;
                background: #f5f5f5;
            }

            .index {
                width: 100vw;
                height: 100vh;
            }

            .main {
                min-height: 30rem;
                width: 100%;
                height: 100%;
                padding-top: 3rem;
                /* background: url('img/f.jpg') !important; */
                background-position: center center;
                background-repeat: no-repeat !important;
                background-size: cover !important;
            }

            td {
                width: 100px;
                height: 50px;
                text-align: center;
            }

            th {
                width: 100px;
                height: 50px;
                text-align: center;
            }

            #studentTable {
                height: 500px !important;
                overflow: auto !important;

            }
        </style>
    </head>

    <body>

        <div class="fixed-top">

            <div class="container-fluid">
                <nav class="navbar navbar-expand-lg navbar-light bg-dark text-white">
                    <a class="navbar-brand text-white" href="#">Navbar</a>
                    <nav class="nav ml-auto">
                        <a class="nav-link disabled" aria-disabled="true"> <span><i class="far fa-user"></i></span> User: <?php echo $username; ?> </a>
                        <a class="nav-link active text-white" href="php/salir.php">Sign out <span><i class="fas fa-sign-out-alt"></i></span></a>


                    </nav>
                </nav>
            </div>

        </div>
        <main class="index">
            <div class="main">
                <div class="container p-5">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active text-dark" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Register Subject</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-dark" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Register students of the course
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-dark" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Register deliverables</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active text-center shadow-lg p-3 mb-5 bg-white rounded" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="container p-5">
                                <div class="container" id="alert">

                                </div>
                                <form id="frmRegisterSubject">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-book"></i></span>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Subject's name" aria-label="Subject's name" id="subject" aria-describedby="basic-addon1">
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Description</span>
                                        </div>
                                        <textarea class="form-control" id="description" aria-label="Description" placeholder="add a description of the course"></textarea>
                                    </div>
                                    <button class="btn btn-primary btn-block mb-3" type="button" id="register_subject">
                                        Register Subject
                                    </button>
                                </form>
                                <hr class="my-4">
                                <div class="container" id="alert1">

                                </div>
                                <form id="frmRegisterST">
                                    <div class="input-group mb-3">
                                        <select class="custom-select" id="subject1">

                                        </select>
                                        <div class="input-group-append">
                                            <label class="input-group-text"><i class="fas fa-book"></i></label>
                                        </div>
                                    </div>
                                    <div class="input-group mb-3">
                                        <select class="custom-select" id="teacher">

                                        </select>
                                        <div class="input-group-append">
                                            <label class="input-group-text" for="inputGroupSelect02"><i class="fas fa-chalkboard-teacher"></i></label>
                                        </div>
                                    </div>
                                    <button class="btn btn-primary btn-block mb-3" type="button" id="register_subject_teacher">
                                        Register subject's teacher
                                    </button>
                                </form>
                            </div>
                        </div>
                        <div class="tab-pane fade text-center shadow-lg p-3 mb-5 bg-white rounded" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="container p-5">
                                <div class="container" id="alert2">

                                </div>
                                <form id="frmSS">

                                    <div class="input-group mb-3">
                                        <select class="custom-select" id="subject2">

                                        </select>
                                        <div class="input-group-append">
                                            <label class="input-group-text"><i class="fas fa-book"></i></label>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="">Choose students</label>
                                        <div class="table-responsive">
                                            <table class="table table-hover table-bordered" id="studentTable">
                                                <thead class="thead-light">
                                                    <tr>
                                                        <th scope="col">#</th>
                                                        <th scope="col">Last</th>
                                                        <th scope="col">first</th>
                                                        <th scope="col">username</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="students">

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <button class="btn btn-primary btn-block mb-3" type="button" id="registerSS">
                                        Save
                                    </button>
                                </form>
                            </div>
                        </div>
                        <div class="tab-pane fade text-center shadow-lg p-3 mb-5 bg-white rounded" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                            <div class="container p-5">
                                <div class="container" id="alert3">

                                </div>
                                <form id="frmDelivereables">
                                    <div class="input-group mb-3">
                                        <select class="custom-select" id="subject3">

                                        </select>
                                        <div class="input-group-append">
                                            <label class="input-group-text"><i class="fas fa-book"></i></label>
                                        </div>
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-archive"></i></span>
                                        </div>
                                        <input type="text" class="form-control" id="name_del" placeholder="Deliverable's name" aria-describedby="basic-addon1">
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Description</span>
                                        </div>
                                        <textarea class="form-control" aria-label="Description" id="description_del" placeholder="add a description of the Deliverable"></textarea>
                                    </div>
                                    <div class="input-group mb-3">

                                        <div class="input-group-prepend" id="deadline">
                                            <span class="input-group-text" id="basic-addon1"><i class="far fa-calendar-alt"></i></span>
                                        </div>
                                        <input type="date" class="form-control" aria-describedby="basic-addon1" id="deadline1">

                                    </div>
                                    <button class="btn btn-primary btn-block mb-3" type="button" id="register_delivereable">
                                        Save
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>


    <script>
        $(document).ready(function() {

            $.ajax({
                type: 'POST',
                url: 'php/load_subjects.php'
            })

            .done(function(subject) {
                $('#subject1').html(subject)
            })
            .fail(function() {
                alert('Hubo un error al cargar los sabyects')
            })

            $.ajax({
                type: 'POST',
                url: 'php/load_subjects.php'
            })

            .done(function(subject) {
                $('#subject2').html(subject)
            })
            .fail(function() {
                alert('Hubo un error al cargar los sabyects')
            })
            $.ajax({
                type: 'POST',
                url: 'php/load_subjects.php'
            })

            .done(function(subject) {
                $('#subject3').html(subject)
            })
            .fail(function() {
                alert('Hubo un error al cargar los sabyects')
            })

            $.ajax({
                type: 'POST',
                url: 'php/load_students.php'
            })

            .done(function(student) {
                $('#students').html(student)
            })
            .fail(function() {
                alert('Hubo un error al cargar los stiudents')
            })

            $.ajax({
                type: 'POST',
                url: 'php/load_teacher.php'
            })
            .done(function(teacher) {
                $('#teacher').html(teacher)
            })
            .fail(function() {
                alert('Hubo un error al cargar los tichers')
            })


        })
    </script>

    <script type="text/javascript">
        $(document).ready(function() {


            $('#register_subject').click(function() {

                if ($('#subject').val() == "") {
                    document.querySelector('#alert').innerHTML = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Please!</strong> You must add subject name.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                    return false;
                }

                cadena = "subject=" + $('#subject').val() +
                "&description=" + $('#description').val();

                $.ajax({
                    type: "POST",
                    url: "php/register_subject.php",
                    data: cadena,
                    success: function(r) {

                        if (r == 2) {
                            document.querySelector('#alert').innerHTML = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Sorry!</strong> This subject already exists.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';

                        } else if (r == 1) {
                            $('#frmRegisterSubject')[0].reset();

                            document.querySelector('#alert').innerHTML = '<div class="alert alert-success alert-dismissible fade show" role="alert"><strong>Congratulations!</strong> Subject have registered correctly.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                        } else {
                            document.querySelector('#alert').innerHTML = '<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>Error!</strong> There was a failure to add.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';

                        }
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: 'php/load_subjects.php'
                })

                .done(function(subject) {
                    $('#subject1').html(subject)
                })
                .fail(function() {
                    alert('Hubo un error al cargar los sabyects')
                })
            });

        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {


            $('#register_subject_teacher').click(function() {

                if ($('#subject1').val() == "0") {
                    document.querySelector('#alert1').innerHTML = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Please!</strong> You must choose a subject name.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                    return false;
                } else if ($('#teacher').val() == "0") {
                    document.querySelector('#alert1').innerHTML = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Please!</strong> You must choose a techaer.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                    return false;
                }

                cadena = "idsubject=" + $('#subject1').val() +
                "&idteacher=" + $('#teacher').val();
                console.log(cadena)

                $.ajax({
                    type: "POST",
                    url: "php/register_subject_teacher.php",
                    data: cadena,
                    success: function(r) {

                        if (r == 2) {
                            document.querySelector('#alert1').innerHTML = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Sorry!</strong> Ya ha registrado el profesor en el curso antes.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';

                        } else if (r == 1) {
                            $('#frmRegisterST')[0].reset();

                            document.querySelector('#alert1').innerHTML = '<div class="alert alert-success alert-dismissible fade show" role="alert"><strong>Congratulations!</strong> Registered correctly.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                        } else {

                            document.querySelector('#alert1').innerHTML = '<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>Error!</strong> There was a failure to register.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';

                        }
                    }
                });
            });

        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {


            $('#registerSS').click(function() {

                if ($('#subject2').val() == "0") {
                    document.querySelector('#alert2').innerHTML = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Please!</strong> You must choose a subject name.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                    return false;
                }

                var rows = document.getElementById("studentTable").rows.length;
                var r = rows.toString();

                cadena = "idsubject=" + $('#subject2').val() +
                "&rows=" + r;

                t = 0;
                for (let index = 0; index < rows; index++) {
                    t = t.toString();
                    if ($(":checkbox:eq(" + index + ")").prop('checked')) {
                        cadena += "&" + t + "=" + $(":checkbox:eq(" + index + ")").val();
                        t++;
                    }
                }
                cadena += "&t=" + t;
                $.ajax({
                    type: "POST",
                    url: "php/enroll_students.php",
                    data: cadena,
                    success: function(r) {

                        if (r >= 1) {
                            $('#frmSS')[0].reset();

                            document.querySelector('#alert2').innerHTML = '<div class="alert alert-success alert-dismissible fade show" role="alert"><strong>Congratulations!</strong> Registered correctly.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                        } else {

                            document.querySelector('#alert2').innerHTML = '<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>Error!</strong> There was a failure to register.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';

                        }
                    }
                });
            });

        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {


            $('#register_delivereable').click(function() {


                if ($('#subject3').val() == "0") {
                    document.querySelector('#alert3').innerHTML = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Please!</strong> You must choose a subject name.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                    return false;
                } else if ($('#name_del').val() == "") {
                    document.querySelector('#alert3').innerHTML = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Please!</strong> You must add a name.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                    return false;
                } else if ($('#description_del').val() == "") {
                    document.querySelector('#alert3').innerHTML = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Please!</strong> You must add a description.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                    return false;
                } else if ($('#deadline1').val() == "") {
                    document.querySelector('#alert3').innerHTML = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Please!</strong> You must add a deadline.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                    return false;
                }

                cadena = "idsub_del=" + $('#subject3').val() +
                "&name_del=" + $('#name_del').val() +
                "&description_del=" + $('#description_del').val() +
                "&deadline=" + $('#deadline1').val();
                console.log(cadena)
                $.ajax({
                    type: "POST",
                    url: "php/register_delivereable.php",
                    data: cadena,


                    success: function(r) {

                        if (r == 2) {
                            document.querySelector('#alert3').innerHTML = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Sorry!</strong> Ya ha registrado el entregable antes.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';

                        } else if (r == 1) {
                            $('#frmDelivereables')[0].reset();

                            document.querySelector('#alert3').innerHTML = '<div class="alert alert-success alert-dismissible fade show" role="alert"><strong>Congratulations!</strong> Registered correctly.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                        } else {

                            document.querySelector('#alert3').innerHTML = '<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>Error!</strong> There was a failure to register.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';

                        }
                    }
                });
            });

        });
    </script>

    </html>
    <?php
} else {
	header("location:login_teacher.php");
}
?>