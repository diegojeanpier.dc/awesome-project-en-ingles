<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>login</title>

    <!-- Google font -->

    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- font awesome -->
    <script src="https://kit.fontawesome.com/e70d054944.js"></script>

    <!-- style -->

    <style>
        body {
            overflow: hidden;
            font-family: 'Ubuntu', sans-serif;
            background: #f5f5f5;
        }
        
        .index {
            width: 100vw;
            height: 100vh;
        }
        
        #main {
            color: white;
            min-height: 30rem;
            width: 100%;
            height: 100%;
            padding-top: 10rem;
            background: url('img/f.jpg') !important;
            background-position: center center;
            background-repeat: no-repeat !important;
            background-size: cover !important;
        }
        
        #rai {
            background-color: transparent !important;
        }
    </style>
</head>

<body>
    <main class="index">
        <header class="d-flex" id="main">
            <div class="container p-5">
                <div class="">
                    <div class="row">
                        <div class="col-2">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <a class="nav-link active shadow-lg mb-3 rounded text-dark" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Log in</a>
                                <a class="nav-link mb-3 shadow-lg rounded text-dark" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Sign up</a>
                            </div>
                        </div>
                        <div id="rai" class="col-10 text-center shadow-lg p-3 mb-5 bg-white rounded">
                            <div class="tab-content" id="v-pills-tabContent">
                                <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                    <div class="container" id="alert1">

                                    </div>
                                    <form id="frmLogin">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                                            </div>
                                            <input type="text" class="form-control" id="usernamelogin" placeholder="Username" aria-label="Username" required>
                                        </div>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-lock"></i></span>
                                            </div>
                                            <input type="password" class="form-control" id="passwordlogin" placeholder="Password" aria-label="Password" required>
                                        </div>
                                        <button class="btn btn-primary btn-block" id="login" type="button">
                                          Log in
                                      </button>
                                  </form>
                              </div>
                              <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                <div class="container" id="alert">

                                </div>
                                <form id="frmRegister">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Name</span>
                                        </div>
                                        <input type="text" name="name" id="name" aria-label="First name" class="form-control" placeholder="First name" required>
                                        <input type="text" name="lastname" id="lastname" aria-label="Last name" class="form-control" placeholder="Last name" required>
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                                        </div>
                                        <input type="text" name="username" id="username" class="form-control" placeholder="Username" aria-label="Username" required>
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-lock"></i></span>
                                        </div>
                                        <input type="password" name="password" id="password" class="form-control" placeholder="Password" aria-label="Password" required>
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-at"></i></span>
                                        </div>
                                        <input type="email" name="email" id="email" class="form-control" placeholder="e-mail" required>
                                    </div>
                                    <button class="btn btn-primary btn-block" type="button" id="register">
                                      Sign up
                                  </button>
                              </form>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>

      <footer class="footer mx-auto text-light text-center fixed-bottom">
        <div class="container">
            <p class="text-light">
                Desarrollado por Desa80 en la 39-b.
            </p>
        </div>

    </footer>
</header>

</main>



<!-- Bootstrap -->
<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>
<script type="text/javascript">
    $(document).ready(function() {
        $('#register').click(function() {

            if($('#username').val()==""){
                document.querySelector('#alert').innerHTML = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Please!</strong> You must add your username.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                return false;
            }else if($('#password').val()==""){
                document.querySelector('#alert').innerHTML = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Please!</strong> You must add your password.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                return false;
            }else if($('#name').val()==""){
                document.querySelector('#alert').innerHTML = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Please!</strong> You must add your name.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                return false;
            }else if($('#lastname').val()==""){
                document.querySelector('#alert').innerHTML = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Please!</strong> You must add your lastname.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                return false;
            }else if($('#email').val()==""){
                document.querySelector('#alert').innerHTML = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Please!</strong> You must add your email.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                return false;
            }

            cadena = "name=" + $('#name').val() +
            "&lastname=" + $('#lastname').val() +
            "&username=" + $('#username').val() +
            "&password=" + $('#password').val() +
            "&email=" + $('#email').val();

            $.ajax({
                type: "POST",
                url: "php/register.php",
                data: cadena,
                success: function(r) {

                    if (r == 2) {
                        document.querySelector('#alert').innerHTML = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Sorry!</strong> This user already exists.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';

                    } else if (r == 1) {
                        $('#frmRegister')[0].reset();

                        document.querySelector('#alert').innerHTML = '<div class="alert alert-success alert-dismissible fade show" role="alert"><strong>Congratulations!</strong> You have registered correctly.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                    } else {
                        document.querySelector('#alert').innerHTML = '<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>Error!</strong> There was a failure to add.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';

                    }
                }
            });
        });
    });
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#login').click(function(){
			if($('#usernamelogin').val()==""){
                document.querySelector('#alert1').innerHTML = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Please!</strong> You must add your username.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';

                return false;
            }else if($('#passwordlogin').val()==""){
                document.querySelector('#alert1').innerHTML = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Please!</strong> You must add your password.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                return false;
            }

            cadena="username=" + $('#usernamelogin').val() + 
            "&password=" + $('#passwordlogin').val();

            $.ajax({
              type:"POST",
              url:"php/login.php",
              data:cadena,
              success:function(r){
                 if(r==1){
                    window.location="inicio.php";
                }else{
                    document.querySelector('#alert1').innerHTML = '<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>Error!</strong> Incorrect username or password.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';

                }
            }
        });
        });	
	});
</script>