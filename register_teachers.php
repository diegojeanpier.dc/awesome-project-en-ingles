<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register teachers</title>

    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- font awesome -->
    <script src="https://kit.fontawesome.com/e70d054944.js"></script>

    <!-- style -->

    <style>
        body {
            font-family: 'Ubuntu', sans-serif;
            background: #f5f5f5;
        }
        
        .index {
            width: 100vw;
            height: 100vh;
        }
        
        #main {
            min-height: 30rem;
            width: 100%;
            height: 100%;
            padding-top: 2rem;
            /* background: url('img/f.jpg') !important; */
            background-position: center center;
            background-repeat: no-repeat !important;
            background-size: cover !important;
        }
        
        #fondo {
            background: transparent;
        }
    </style>

</head>

<body>

    <main class="index">
        <div class="d-flex" id="main">
            <div class="container p-5">
                <div class="jumbotron" id=fondo>
                    <h1 class="display-4">Register teachers</h1>
                    <!-- <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p> -->
                    <hr class="my-4">
                    <div class="form">
                        <div class="container" id="alert">

                        </div>
                        <form id="frmRegister">

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Name</span>
                                </div>
                                <input type="text" aria-label="First name" id="name" class="form-control" placeholder="First name" required>
                                <input type="text" aria-label="Last name" id="lastname" class="form-control" placeholder="Last name" required>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                                </div>
                                <input type="text" class="form-control" id="username" placeholder="Username" aria-label="Username" required>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-lock"></i></span>
                                </div>
                                <input type="password" class="form-control" id="password" placeholder="Password" aria-label="Password" required>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-at"></i></span>
                                </div>
                                <input type="email" class="form-control" id="email" placeholder="e-mail" required>
                            </div>
                            <button class="btn btn-primary btn-block" id="register" type="button">
                              Sign up
                          </button>
                      </form>

                  </div>
              </div>
          </div>
      </div>
  </main>

  <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>
<script type="text/javascript">
    $(document).ready(function() {
        $('#register').click(function() {

            if ($('#username').val() == "") {
                document.querySelector('#alert').innerHTML = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Please!</strong> You must add your username.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';

                return false;
            } else if ($('#password').val() == "") {
                document.querySelector('#alert').innerHTML = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Please!</strong> You must add your password.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                return false;
            } else if ($('#name').val() == "") {
                document.querySelector('#alert').innerHTML = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Please!</strong> You must add your name.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                return false;
            } else if ($('#lastname').val() == "") {
                document.querySelector('#alert').innerHTML = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Please!</strong> You must add your lastname.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                return false;
            } else if ($('#email').val() == "") {
                document.querySelector('#alert').innerHTML = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Please!</strong> You must add your email.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                return false;
            }

            cadena = "name=" + $('#name').val() +
            "&lastname=" + $('#lastname').val() +
            "&username=" + $('#username').val() +
            "&password=" + $('#password').val() +
            "&email=" + $('#email').val();

            $.ajax({
                type: "POST",
                url: "php/register_teacher.php",
                data: cadena,
                success: function(r) {

                    if (r == 2) {
                        document.querySelector('#alert').innerHTML = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Sorry!</strong> This user already exists.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';

                    } else if (r == 1) {
                        $('#frmRegister')[0].reset();

                        document.querySelector('#alert').innerHTML = '<div class="alert alert-success alert-dismissible fade show" role="alert"><strong>Congratulations!</strong> You have registered correctly.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                    } else {
                        document.querySelector('#alert').innerHTML = '<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>Error!</strong> There was a failure to add.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';

                    }
                }
            });
        });
    });
</script>