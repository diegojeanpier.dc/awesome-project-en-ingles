<?php 
session_start();

if(isset($_SESSION['user_student'])){
	$username = $_SESSION['user_student'];
	?>

	<!DOCTYPE html>
	<html lang="en">

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title>Main</title>

		<!-- Google font -->

		<link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">

		<!-- Bootstrap -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

		<!-- font awesome -->
		<script src="https://kit.fontawesome.com/e70d054944.js"></script>

		<!-- style -->

		<style>
			body {

				font-family: 'Ubuntu', sans-serif;
				background: #f5f5f5;
			}

			.index {
				width: 100vw;
				height: 100vh;
			}

			#main {
				min-height: 5rem;
				width: 100%;
				height: 100%;
				padding-top: 5rem;
				/* background: url('img/f.jpg') !important; */
				background-position: center center;
				background-repeat: repeat !important;
				background-size: cover !important;
			}

			.nav-pills .nav-link.active, .nav-pills .show>.nav-link {
				color: #fff !important;
				background-color: #007bff;
			}
		</style>
	</head>

	<body>

		<div class="fixed-top">
			<div class="container-fluid">
				<nav class="navbar navbar-expand-lg navbar-light bg-dark text-white">
					<a class="navbar-brand text-white" href="#">Navbar</a>
					<nav class="nav ml-auto">
						<a class="nav-link disabled" aria-disabled="true"> <span><i class="far fa-user"></i></span> User: <?php echo $username; ?> </a>
						<a class="nav-link active text-white" href="php/salir.php">Sign out <span><i class="fas fa-sign-out-alt"></i></span></a>
						<input type="text" id="username" value="<?php echo $username; ?>" hidden>
					</nav>
				</nav>
			</div>
		</div>
		<main class="index">

			<header class="d-flex" id="main">
				<div class="container">
					<div class="row">
						<div class="col-2" id='courses'>

						</div>
						<div class="col-10 text-center shadow-lg p-3 mb-5 bg-white rounded">

							<div class="tab-content" id="v-pills-tabContent">
								<div class="tab-pane fade show active" id="v-pills-ADS" role="tabpanel" aria-labelledby="v-pills-ADS-tab">
									<ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="groups-tab" data-toggle="tab" href="#groups" role="tab" aria-controls="groups" aria-selected="true">Group</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="delivereables-tab" data-toggle="tab" href="#delivereables" role="tab" aria-controls="delivereables" aria-selected="false">Deliverables</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="list-tab" data-toggle="tab" href="#list" role="tab" aria-controls="list" aria-selected="false">Student list</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="announcements-tab" data-toggle="tab" href="#announcements" role="tab" aria-controls="announcements" aria-selected="false">Announcements</a>
                                        </li>
                                    </ul>
								</div>
								<div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">...</div>
								<div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">...</div>
							</div>
							
						</div>	
					</div>
				</div>
			</header>
		</main>   	

		<!-- Bootstrap y jotaquery -->
		<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	</body>
	
	</html>
	<script type="text/javascript">
		$(document).ready(function(){
			var username = $('#username').val();
			$.ajax({
				type:'POST',
				url:'php/load_student_subject.php',
				data:{'username':username}
			})
			.done(function(courses){
				$('#courses').html(courses)
			})
			.fail(function(){
				alert('Hubo un error al cargar los cursos')
			})
		})
	</script>
	<?php
} else {
	header("location:login.php");
}
?>