<?php 
session_start();

if(isset($_SESSION['user_student'])){
    $username = $_SESSION['user_student'];
    ?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Main</title>

        <!-- Google font -->

        <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <!-- font awesome -->
        <script src="https://kit.fontawesome.com/e70d054944.js"></script>

        <!-- style -->

        <style>
            body {

                font-family: 'Ubuntu', sans-serif;
                background: #f5f5f5;
            }

            .index {
                width: 100vw;
                height: 100vh;
            }

            #main {
                min-height: 5rem;
                width: 100%;
                height: 100%;
                padding-top: 5rem;
                /* background: url('img/f.jpg') !important; */
                background-position: center center;
                background-repeat: repeat !important;
                background-size: cover !important;
            }
        </style>
    </head>


    <body>
        <div class="fixed-top">

            <div class="container-fluid">
                <nav class="navbar navbar-expand-lg navbar-light bg-dark text-white">
                    <a class="navbar-brand text-white" href="#">Navbar</a>
                    <nav class="nav ml-auto">
                        <a class="nav-link disabled" aria-disabled="true"> <span><i class="far fa-user"></i></span> User: <?php echo $username; ?> </a>
                        <a class="nav-link active text-white" href="php/salir.php">Sign out <span><i class="fas fa-sign-out-alt"></i></span></a>


                    </nav>
                </nav>
            </div>





        </div>
        <main class="index">

            <header class="d-flex" id="main">

                <?php
                if(isset($_SESSION['user_student'])){
                 echo '
                    <div class="container">
                    <div class="container">
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>Congratuleishons</strong> You haber ingresado with exito.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                 ';

                }
                ?>

                    <div class="row">
                        <div class="col-2">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <a class="nav-link active text-dark" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Course's name 1</a>
                                <a class="nav-link text-dark" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Course's name 2</a>
                                <a class="nav-link text-dark" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Course's name 3</a>
                                <a class="nav-link text-dark" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Course's name 4</a>
                            </div>
                        </div>
                        <div class="col-10 text-center shadow-lg p-3 mb-5 bg-white rounded">
                            <div class="tab-content" id="v-pills-tabContent">
                                <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="groups-tab" data-toggle="tab" href="#groups" role="tab" aria-controls="groups" aria-selected="true">Group</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="delivereables-tab" data-toggle="tab" href="#delivereables" role="tab" aria-controls="delivereables" aria-selected="false">Deliverables</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="list-tab" data-toggle="tab" href="#list" role="tab" aria-controls="list" aria-selected="false">Student list</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="announcements-tab" data-toggle="tab" href="#announcements" role="tab" aria-controls="announcements" aria-selected="false">Announcements</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" id="groups" role="tabpanel" aria-labelledby="groups-tab">
                                            <div class="jumbotron bg-white">
                                                <h1 class="display-4">Group 1</h1>
                                                <p class="lead">Group's name</p>
                                                <hr class="my-4">
                                                <p>Member list.</p>
                                                <div class="table-responsive">
                                                    <table class="table table-hover table-bordered">
                                                        <thead class="thead-light">
                                                            <tr>
                                                                <th scope="col">#</th>
                                                                <th scope="col">First</th>
                                                                <th scope="col">Last</th>
                                                                <th scope="col">username</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th scope="row">Coordinator</th>
                                                                <td>Mark</td>
                                                                <td>Otto</td>
                                                                <td>@mdo</td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Member</th>
                                                                <td>Jacob</td>
                                                                <td>Thornton</td>
                                                                <td>@fat</td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Member</th>
                                                                <td>Larry the Bird</td>
                                                                <td>@twitter</td>
                                                                <td>asd</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="delivereables" role="tabpanel" aria-labelledby="delivereables-tab">
                                            <div class="container">
                                                <div class="delivereables mt-3">
                                                    <div class="card mb-3">
                                                        <div class="card-header">
                                                            Deliverable 1
                                                        </div>
                                                        <div class="card-body">
                                                            <h5 class="card-title">Description delivereable</h5>
                                                            <p class="card-text">Deadline delivery: 17-05-2019 23:59:59</p>
                                                            <div class="alert alert-success" role="alert">
                                                                Delivered!
                                                            </div>
                                                        </div>
                                                        <div class="card-footer text-muted">
                                                            2 days ago
                                                        </div>
                                                    </div>
                                                    <div class="card mb-3">
                                                        <div class="card-header">
                                                            Deliverable 2
                                                        </div>
                                                        <div class="card-body">
                                                            <h5 class="card-title">Description delivereable</h5>
                                                            <p class="card-text">Deadline delivery: 17-06-2019 23:59:59</p>
                                                            <div class="alert alert-warning" role="alert">
                                                                You have not delivered it yet!
                                                            </div>
                                                            <label for="">Send your file</label>
                                                            <div class="input-group">
                                                                <div class="custom-file">
                                                                    <input type="file" class="custom-file-input" id="inputGroupFile04" accept=".pdf, .docx" aria-describedby="inputGroupFileAddon04">
                                                                    <label class="custom-file-label" for="inputGroupFile04">Choose file</label>
                                                                </div>
                                                                <div class="input-group-append">
                                                                    <button class="btn btn-outline-secondary" type="button" id="inputGroupFileAddon04">Send</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="card-footer text-muted">
                                                            2 days ago
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="list" role="tabpanel" aria-labelledby="list-tab">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-bordered">
                                                    <thead class="thead-light">
                                                        <tr>
                                                            <th scope="col">#</th>
                                                            <th scope="col">First</th>
                                                            <th scope="col">Last</th>
                                                            <th scope="col">username</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th scope="row">1</th>
                                                            <td>Mark</td>
                                                            <td>Otto</td>
                                                            <td>@mdo</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">2</th>
                                                            <td>Jacob</td>
                                                            <td>Thornton</td>
                                                            <td>@fat</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">3</th>
                                                            <td>Larry the Bird</td>
                                                            <td>@twitter</td>
                                                            <td>asd</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="announcements" role="tabpanel" aria-labelledby="announcements-tab">
                                            <div class="accordion mt-3" id="accordionExample">
                                                <div class="card">
                                                    <div class="card-header" id="headingOne">
                                                        <h2 class="mb-0">
                                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                                Announcement #1
                                                            </button>
                                                        </h2>
                                                    </div>

                                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                                        <div class="card-body">
                                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird
                                                            on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.
                                                            Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-header" id="headingTwo">
                                                        <h2 class="mb-0">
                                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                                Announcement #2
                                                            </button>
                                                        </h2>
                                                    </div>
                                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                                        <div class="card-body">
                                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird
                                                            on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.
                                                            Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-header" id="headingThree">
                                                        <h2 class="mb-0">
                                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                                Announcement #3
                                                            </button>
                                                        </h2>
                                                    </div>
                                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                                        <div class="card-body">
                                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird
                                                            on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.
                                                            Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="groups-tab" data-toggle="tab" href="#groups1" role="tab" aria-controls="groups" aria-selected="true">Groups</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="delivereables-tab" data-toggle="tab" href="#delivereables1" role="tab" aria-controls="delivereables" aria-selected="false">Deliverables</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="list-tab" data-toggle="tab" href="#list1" role="tab" aria-controls="list" aria-selected="false">Student list</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="announcements-tab" data-toggle="tab" href="#announcements1" role="tab" aria-controls="announcements" aria-selected="false">Announcements</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" id="groups1" role="tabpanel" aria-labelledby="groups-tab">
                                            <p>You have no registered group.</p>
                                            <p>
                                                <a class="btn btn-info" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">Create group</a>

                                            </p>
                                            <div class="row">
                                                <div class="col">
                                                    <div class="collapse multi-collapse" id="multiCollapseExample1">
                                                        <div class="card card-body">
                                                            <form action="">
                                                                <div class="input-group mb-3">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text" id="basic-addon1"><i class="fas fa-users"></i></span>
                                                                    </div>
                                                                    <input type="text" class="form-control" placeholder="Group name" aria-label="Group name" aria-describedby="basic-addon1">
                                                                </div>
                                                                <div class="mb-3">
                                                                    <label for="">Choose students</label>

                                                                    <div class="table-responsive">
                                                                        <table class="table table-hover table-bordered">
                                                                            <thead class="thead-light">
                                                                                <tr>
                                                                                    <th scope="col">#</th>
                                                                                    <th scope="col">First</th>
                                                                                    <th scope="col">Last</th>
                                                                                    <th scope="col">username</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td>
                                                                                        <input type="checkbox" class="custom-control-input">
                                                                                    </td>
                                                                                    <td>Mark</td>
                                                                                    <td>Otto</td>
                                                                                    <td>@mdo</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><input type="checkbox" class="custom-control-input"></td>
                                                                                    <td>Jacob</td>
                                                                                    <td>Thornton</td>
                                                                                    <td>@fat</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><input type="checkbox" class="custom-control-input"></td>
                                                                                    <td>Larry the Bird</td>
                                                                                    <td>@twitter</td>
                                                                                    <td>asd</td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                                <button class="btn btn-primary btn-block mb-3">
                                                                    Save Group
                                                                </button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="delivereables1" role="tabpanel" aria-labelledby="delivereables-tab">Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident nam autem totam aperiam deleniti dolor amet tempora illo earum dolores rem ab facilis natus doloremque, dolorum maxime ratione. Unde, in.</div>
                                        <div class="tab-pane fade" id="list1" role="tabpanel" aria-labelledby="list-tab">Lorem ipsum dolor sit amet consectetur, adipisicing elit. In reprehenderit culpa doloribus saepe optio distinctio, obcaecati, nesciunt nostrum consequatur vero autem. Blanditiis modi amet ea. Fugit nisi quas dicta impedit.</div>
                                        <div class="tab-pane fade" id="announcements1" role="tabpanel" aria-labelledby="announcements-tab">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis et unde velit, rerum magnam similique earum dicta doloremque reprehenderit ut nostrum minima minus odit molestiae quisquam, sapiente neque deleniti aliquid!</div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">...</div>
                                <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">...</div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        </main>



        <!-- Bootstrap -->
        <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>

    </html>
    <?php
} else {
	header("location:login.php");
}
?>