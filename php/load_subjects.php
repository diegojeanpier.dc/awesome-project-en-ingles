<?php
require_once 'conexion.php';

function getSubjects(){
    $mysqli = conexion();
    $query = 'SELECT * FROM subject';
    $result = $mysqli->query($query);
    $subject='
    <option value="0">Choose a subject</option>';
    while ($row = $result->fetch_array(MYSQLI_ASSOC)){
        $subject .= '<option value ="'.$row[idsubject].'">'.$row[name].'</option>';
    } 
    return $subject;
}

echo getSubjects();
?>