<?php
require_once 'conexion.php';

function getTeachers(){
    $mysqli = conexion();
    $query = 'SELECT * FROM teacher';
    $result = $mysqli->query($query);
    $teacher='
    <option value="0">Choose a teacher</option>';
    while ($row = $result->fetch_array(MYSQLI_ASSOC)){
        $teacher .= '<option value = "'.$row[idteacher].'">'.$row[name].' '.$row[lastname].'</option>';
    } 
    return $teacher;
}

echo getTeachers();
?>